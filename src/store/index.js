import { createStore } from "vuex";
import access from "@/modules/access/store";
import home from "@/modules/home/store";

const store = createStore({
    modules: {
      access,
      home
    }
})

export default store