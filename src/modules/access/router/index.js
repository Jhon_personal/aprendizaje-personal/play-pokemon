export default {

    name: 'access',
    component: () => import(/* webpackChunkName: "access" */ '@/modules/access/layouts/Access.vue'),
    children: [
        {
            path: '/',
            name: 'login',
            component: () => import(/* webpackChunkName: "login" */ '@/modules/access/views/Login.vue')
        },
        {
            path: '/register',
            name: 'register',
            component: () => import(/* webpackChunkName: "register" */ '@/modules/access/views/Register.vue')
        },
        {
            path: '/recovery',
            name: 'recovery',
            component: () => import(/* webpackChunkName: "recovery" */ '@/modules/access/views/Recovery.vue')
        }
    ]
}