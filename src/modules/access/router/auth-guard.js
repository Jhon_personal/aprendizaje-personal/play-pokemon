import store from "@/store";

const isToken = async(to, from, next) => {

    const { status } = await store.dispatch('access/checkToken')

    if(status) next()
    else next({name: 'login'})
}

export default isToken