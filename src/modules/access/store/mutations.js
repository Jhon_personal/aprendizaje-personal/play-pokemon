export const setDataUser = (state, { dataUser } ) => {

    state.isLoading = false
    state.nameUser = dataUser.usuario 
    state.correoUser = dataUser.correo

    localStorage.setItem('idToken', dataUser.token)
}

export const logout = (state) => {

    state.isLoading = true
    state.nameUser = null
    state.correoUser = null
    localStorage.removeItem('idToken')
}