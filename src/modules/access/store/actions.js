import authApi from "@/api/authApi"

export const TokenUser = async( ) => {

    const { data } = await authApi.post('token', { })
  
    return data
}

export const RegisterUser = async( {commit}, { token, user }) => {

    const { usuario, pass, correo} = user

    const headers = {
        'Authorization': 'Bearer ' + token,
    };

    const { data } = await authApi.post('register', { CORREO: correo, USUARIO: usuario, PASSWORD: pass}, { headers })

    const { dataUser } = data

    commit('setDataUser', { dataUser } )

    return data
}

export const LoginUser = async({commit}, {token, user}) => {

    const { usuario, pass} = user

    const headers = {
        'Authorization': 'Bearer ' + token,
    };

    const { data } = await authApi.post('login', { USUARIO: usuario, PASSWORD: pass}, { headers })

    const { dataUser } = data
   
    commit('setDataUser', { dataUser } )

    return data
}

export const checkToken = async({commit}) => {

    const idToken = localStorage.getItem('idToken')

    if(!idToken){

        commit('logout')

        return { status: false}
    }

    try{

        const headers = {
            'Authorization': 'Bearer ' + idToken,
        };

        const { data } = await authApi.get(`tokenInfo/${idToken}`, { headers })

        const { dataUser } = data

        commit('setDataUser', { dataUser } )

        return { status: true}

    } catch (error) {

        commit('logout')

        return { status: false}
    }
}

export const recoveryPass = async({}, {token, correo}) => {

    const headers = {
        'Authorization': 'Bearer ' + token,
    };

    const { data } = await authApi.post('recovery', { CORREO: correo}, { headers })

    return data
}