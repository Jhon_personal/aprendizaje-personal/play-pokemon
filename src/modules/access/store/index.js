import * as actions  from './actions'
import state from './state'
import * as mutations  from './mutations'
import * as getters  from './getters'



const accessModule = {
    namespaced: true,
    actions,
    mutations,
    getters,
    state

}

export default accessModule