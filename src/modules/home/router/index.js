export default {
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ '@/modules/home/layouts/Home.vue'),
    children: [
        {
            path: '/newGame',
            name: 'newGame',
            component: () => import(/* webpackChunkName: "newGame" */ '@/modules/home/views/newGame.vue')
        },
    ]
}