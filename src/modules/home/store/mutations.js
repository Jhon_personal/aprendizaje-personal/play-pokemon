export const setPoint = (state, { point } ) => {

    state.totalPoints = state.totalPoints + point

}

export const setPointDefault = (state) => {

    state.totalPoints = 0

}