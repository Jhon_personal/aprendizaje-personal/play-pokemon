import * as actions  from './actions'
import state from './state'
import * as mutations  from './mutations' 
import * as getters  from './getters'


const homeModule = {
    namespaced: true,
    actions,
    state,
    mutations, 
    getters

}

export default homeModule