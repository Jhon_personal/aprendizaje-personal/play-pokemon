import pokemonApi from "@/api/pokemonApi"
import authApi from "@/api/authApi"

export const pokemonInfo = async( ) => {

    const idPokemon = Math.floor(Math.random() * 1017) + 1;

    const { data } = await pokemonApi.get(`https://pokeapi.co/api/v2/pokemon/${idPokemon}`)

    return data
}

export const pokemonsRandom = async() => {

    const pokemons = [];

    for (let i = 1; i <= 3; i++) {

        const idPokemon = Math.floor(Math.random() * 1017) + 1;
        const { data } = await pokemonApi.get(`https://pokeapi.co/api/v2/pokemon/${idPokemon}`);
        const { name, id } = data

        pokemons.push({name, id});

    }

  return pokemons;
}

export const setSelectedPokemon = async({ commit }, {pokemon, idPokemon, namePokmeon}) => {

    const msgSelected = {};
    const point = 1

    if (pokemon.id === idPokemon.value) {

        msgSelected.state = 'ok';
        msgSelected.msg = `Correcto, es ${namePokmeon.value}`;

        commit('setPoint', { point } )
      } else {
        msgSelected.state = 'failed';
        msgSelected.msg = `Error, era ${namePokmeon.value}`;
      }

    return msgSelected
}

export const savePoints = async({ commit }, {pointsGame, userMail, token}) => {
  
  if(pointsGame.value > 0){

    const headers = {
      'Authorization': 'Bearer ' + token,
    };

    let fecha = new Date()
    let year = fecha.getFullYear();
    let month = String(fecha.getMonth() + 1).padStart(2, '0');
    let day = String(fecha.getDate()).padStart(2, '0');

    const fechaNow = year + '-' + month + '-' + day

    
    const { data } = await authApi.post('points', { CORREO: userMail.value, PUNTAJE: pointsGame.value, FECHA: fechaNow}, { headers })
    
    commit('setPointDefault')
    return data
  }else{

    commit('setPointDefault')
    return ''
  }
}

export const listRanking = async({ commit }, {token}) => {

  const headers = {
    'Authorization': 'Bearer ' + token,
  };

  const { data } = await authApi.get('list', { headers })
  
  return data
}

export const dataPointsUser = async({}, {token, user}) => {

  const headers = {
    'Authorization': 'Bearer ' + token,
  };

  const { data } = await authApi.get(`pointsUser/${user}`, { headers })
  
  return data
}