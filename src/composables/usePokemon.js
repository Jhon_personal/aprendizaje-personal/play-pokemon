import { useStore } from "vuex"

const usePokemon = ( ) => {

    const store = useStore()
    const pokemonInfo = async() => {

        const data = await store.dispatch('home/pokemonInfo');

        return data
    }

    const pokemonsRandom = async() => {

        const data = await store.dispatch('home/pokemonsRandom');

        return data
    }

    const setSelectedPokemon = async(pokemon, idPokemon, namePokmeon) => {

        const data = await store.dispatch('home/setSelectedPokemon', { pokemon, idPokemon, namePokmeon });

        return data
    }

    return{
        pokemonInfo,
        pokemonsRandom,
        setSelectedPokemon
    }
}

export default usePokemon