import { computed } from "vue";
import { useStore } from "vuex"

const useUsers = ( ) => {

    const store = useStore()
    const RegisterUser = async(user) => {
 
        const token = await store.dispatch('access/TokenUser');

        const data = await store.dispatch('access/RegisterUser', { token, user });
 
        return data;
    }
    
    const LoginUser = async(user) => {

        const token = await store.dispatch('access/TokenUser');

        const data = await store.dispatch('access/LoginUser', { token, user });

        return data;
    }

    const logout = async() => {

        store.commit('access/logout')
    }

    const recoveryPass = async(correo) => {

        const token = await store.dispatch('access/TokenUser');
        
        const data = await store.dispatch('access/recoveryPass', { token, correo });

        return data;
    }

    return{
        RegisterUser,
        LoginUser,
        logout,
        recoveryPass,

        nameUser: computed(() => store.getters['access/nameUser'] )
    }
}

export default useUsers