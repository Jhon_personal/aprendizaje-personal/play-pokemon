import { useStore } from "vuex"
import { computed } from "vue";

const usePoints = ( ) => {

    const store = useStore()

    const savePoints = async(pointsGame, userMail) => {

        const token = await store.dispatch('access/TokenUser');

        const data = await store.dispatch('home/savePoints', {pointsGame, userMail, token})

        return data
    }

    const listRanking = async() => {

        const token = await store.dispatch('access/TokenUser');

        const data = await store.dispatch('home/listRanking', {token});

        return data
    }

    const dataPointsUser = async(user) => {

        const token = await store.dispatch('access/TokenUser');

        const data = await store.dispatch('home/dataPointsUser', {token, user});

        return data
    }

    return{
        savePoints,
        listRanking,
        dataPointsUser, 

        pointsUser: computed(() => store.getters['home/pointsUser'] ), 
        correoUser: computed(() => store.getters['access/correoUser'] ),
        nameUser: computed(() => store.getters['access/nameUser'] )
    }
}

export default usePoints