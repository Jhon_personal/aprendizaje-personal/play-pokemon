import { createRouter, createWebHashHistory } from 'vue-router'
import accessRouter from "@/modules/access/router";
import homeRouter from "@/modules/home/router";
import isToken from '@/modules/access/router/auth-guard';

const routes = [
  {
    path: '/',
    ...accessRouter,
  },
  {
    path: '/home',
    beforeEnter: [isToken],
    ...homeRouter,
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
